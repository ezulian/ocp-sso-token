"""Obtain an OCP OAuth token for an SSO IdP with Kerberos support."""

__version__ = '1.3.0'
